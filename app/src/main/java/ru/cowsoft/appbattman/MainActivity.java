package ru.cowsoft.appbattman;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.app.AppOpsManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class MainActivity extends AppCompatActivity
{
    private Context mContext;
    private Set<String> requiredPackages;
    private SharedPreferences sharedpreferences;
    private LayoutInflater inflater;
    public static final String mypreference = "appbattman";
    public static final String PREF_SET = "pref_set";

    static class BattMode
    {
        static final int DISABLE = 0, ENABLE = 1;
    }

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        try
        {
            super.onCreate( savedInstanceState );
            setContentView( R.layout.activity_main );
            mContext = getApplicationContext();

            inflater = (LayoutInflater) mContext.getSystemService( LAYOUT_INFLATER_SERVICE );
            sharedpreferences = mContext.getSharedPreferences( mypreference, Context.MODE_PRIVATE );
            Set<String> readPackages = sharedpreferences.getStringSet( PREF_SET, new HashSet<>() );
            requiredPackages = new HashSet<>( readPackages );

            LinearLayout main_table = findViewById( R.id.main_linear_layout );
            PackageManager manager = getPackageManager();

            for( ApplicationInfo app : manager.getInstalledApplications( PackageManager.GET_META_DATA ) )
            {
                if( app.sourceDir.startsWith( "/data/app/" ) && manager.getLaunchIntentForPackage( app.packageName ) != null )
                {
                    ApplicationInfo applicationInfo = manager.getApplicationInfo( app.packageName, 0 );
                    CharSequence label = manager.getApplicationLabel( applicationInfo );
                    Resources resources = manager.getResourcesForApplication( app.packageName );
                    setButtonRows( mContext, resources, main_table, label, app.packageName, app.icon );
                }
            }
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
    }

    void setButtonRows( final Context ctx,
                        Resources resource,
                        LinearLayout parentLinearLayout,
                        CharSequence big_text,
                        String packageName,
                        int img_id )
    {
        View childLayout = inflater.inflate( R.layout.row_layout2, null );

        TextView textBig = childLayout.findViewById( R.id.text_big );
        TextView textSmall = childLayout.findViewById( R.id.text_small );
        ImageView imageView = childLayout.findViewById( R.id.item_icon );
        Switch batt_switch = childLayout.findViewById( R.id.batt_switch );

        textBig.setText( big_text );
        textSmall.setText( packageName );

        if( img_id != 0 )
        {
            imageView.setImageDrawable( ResourcesCompat.getDrawable( resource, img_id, null ) );
        }

        if( requiredPackages.contains( packageName ) )
        {
            batt_switch.setChecked( true );
        }

        batt_switch.setOnCheckedChangeListener( ( buttonView, isChecked ) ->
        {
            if( isChecked )
            {
                requiredPackages.add( packageName );
                // Disable battery restrictions
                setBatteryOpt( mContext, packageName, BattMode.DISABLE );
            }
            else
            {
                requiredPackages.remove( packageName );
                // Enable battery restrictions
                setBatteryOpt( mContext, packageName, BattMode.ENABLE );
            }

            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putStringSet( PREF_SET, requiredPackages );
            editor.apply();
            editor.commit();
        } );

        parentLinearLayout.addView( childLayout );
    }

    public static void setBatteryOpt( Context mContext, String packageName, int value )
    {
        try
        {
            // 128 == 1000 0000
            int uid = mContext.getPackageManager().getPackageUid( packageName, PackageManager.GET_META_DATA );

            AppOpsManager appOpsManager = (AppOpsManager) mContext.getSystemService( Context.APP_OPS_SERVICE );
            Class.forName( "android.app.AppOpsManager" )
                    .getMethod( "setMode", Integer.TYPE, Integer.TYPE, String.class, Integer.TYPE )
                    .invoke( appOpsManager, 70, uid, packageName, value );
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
    }
}
