package ru.cowsoft.appbattman;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import java.util.HashSet;
import java.util.Set;

import static ru.cowsoft.appbattman.MainActivity.PREF_SET;
import static ru.cowsoft.appbattman.MainActivity.mypreference;

public class CowBoot extends BroadcastReceiver
{
    @Override
    public void onReceive( Context context, Intent intent )
    {
        try
        {
            if( intent.getAction().equals( Intent.ACTION_BOOT_COMPLETED ) )
            {
                SharedPreferences sharedpreferences = context.getSharedPreferences( mypreference, Context.MODE_PRIVATE );
                Set<String> readPackages = sharedpreferences.getStringSet( PREF_SET, new HashSet<>() );
                HashSet<String> requiredPackages = new HashSet<>( readPackages );

                for( String packageName: requiredPackages )
                {
                    MainActivity.setBatteryOpt( context, packageName, MainActivity.BattMode.DISABLE );
                }

                Toast.makeText( context, "KSW Battery settings applied", Toast.LENGTH_SHORT ).show();
            }
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
    }
}

